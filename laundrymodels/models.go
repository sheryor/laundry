package laundrymodels

// Config struct contains all information about laundry services and their prices
type Config struct {
	LandryPriceList              []Clothes     `json:"laundryPriceList"`
	GeneralPriceList             []Clothes     `json:"generalPriceList"`
	IroningPriceList             []Clothes     `json:"ironingPriceList"`
	StainRemovalServicePriceList []Clothes     `json:"stainRemovalPriceList"`
	OtherServicePriceList        OtherServices `json:"otherServicePriceList"`
}

// Laundry struct contains all information only about laundry services
type Laundry struct {
	Coat         float32 `json:"coat"`
	Trouser      float32 `json:"trouser"`
	Suit         float32 `json:"suit"`
	Frock        float32 `json:"frock"`
	Dress        float32 `json:"dress"`
	WeddingDress float32 `json:"weddingDress"`
}

// GeneralService struct contains all information about only GeneralServices
type GeneralService struct {
	WhiteClothes    float32 `json:"whiteClothes"`
	ColorfulClothes float32 `json:"colorfulClothes"`
	Wool            float32 `json:"wool"`
	SilkClothes     float32 `json:"silkClothes"`
	SoftToy         float32 `json:"softToy"`
	Linens          float32 `json:"linens"`
}

// IroningService struct contains all information about  only Ironing services
type IroningService struct {
	Shirt    float32 `json:"shirt"`
	Trousers float32 `json:"trousers"`
	Skirt    float32 `json:"skirt"`
	Dress    float32 `json:"dress"`
	Suit     float32 `json:"suit"`
}

// StainRemovalService struct contains all information about only SainRemovalService
type StainRemovalService struct {
	Oil         float32 `json:"oil"`
	Blood       float32 `json:"blood"`
	GeneralDirt float32 `json:"generalDirt"`
}

// OtherService remain services of company
type OtherServices struct {
	RepairClose    float32 `json:"repairClose"`
	TwentyOffPrice int32   `json:"twentyOffPrice"`
}

type Clothes struct {
	Name    string  `json:"name"`
	Desc    string  `json:"desc"`
	Price   float32 `json:"price"`
	Measure string  `json:"measure"`
	Count   float32 `json:"count,omitempty"`
	Sum     float32 `json:"sum,omitempty"`
}

// incoming request
type IncomingReq struct {
	Order    []Clothes       `json:"order"`
	Optional OptionalService `json:"optional"`
}

type OptionalService struct {
	IsExpress      bool `json:"isExpress"`
	IsChildClothes bool `json:"isChildClothes"`
	IsSlow         bool `json:"isSlow"`
}
