package laundryutils

import (
	"encoding/json"
	"io/ioutil"
	"laundry/laundrymodels"
)

// ReadConfig reading pricelist from json file
func ReadConfig() laundrymodels.Config {
	var config laundrymodels.Config
	doc, err := ioutil.ReadFile("./assets/config.json")

	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(doc, &config)
	if err != nil {
		panic(err)
	}
	return config
}
