package laundry_rest

import (
	"fmt"
	"laundry/laundrymodels"
	"laundry/laundryutils"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	PT = "pt"
	KG = "kg"
)

// GetConfig getting pricelist basic values
func GetConfig(c *gin.Context) {
	configs := laundryutils.ReadConfig()
	c.JSON(http.StatusOK, configs)
}

func Checkout(c *gin.Context) {
	var req laundrymodels.IncomingReq
	var kg float32
	var totalSum float32

	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err})
		return
	}

	orders := req.Order
	totalSum = 0
	file, err := os.OpenFile("checkout.log", os.O_WRONLY|os.O_APPEND, 0644)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err})
		return
	}

	defer file.Close()

	currentTime := time.Now()

	dateInput := currentTime.Format("2006.01.02 15:04:05")
	file.WriteString("======================================================\n")
	file.WriteString("-------------------------------------------\n")
	file.WriteString(fmt.Sprintf("|Время заказа:%s|\n", dateInput))
	file.WriteString("-------------------------------------------\n")
	for _, order := range orders {
		if order.Measure == KG {
			kg += order.Count
		}
		inputString := fmt.Sprintf("%s колво:%f цена:%f\n", order.Desc, order.Count, order.Price)
		file.WriteString(inputString)
		totalSum += order.Count * order.Price
	}
	file.WriteString("-------------------------------------------\n")

	if kg > 10 {
		totalSum *= 0.8
		file.WriteString("Скидка больше 10кг одежды - 20%\n")
		file.WriteString("-------------------------------------------\n")
	}

	if req.Optional.IsExpress == true {
		totalSum *= 1.5
		file.WriteString("Надбавка цены за экспресс - 50%\n")
		file.WriteString("-------------------------------------------\n")
	}

	if req.Optional.IsSlow == true {
		totalSum *= 0.7
		file.WriteString("Скидка за ожидание 5 и более дней - 30%\n")
		file.WriteString("-------------------------------------------\n")
	}

	if req.Optional.IsChildClothes == true {
		totalSum *= 0.5
		file.WriteString("Скидка деткская одежда - 50%\n")
		file.WriteString("-------------------------------------------\n")
	}

	file.WriteString(fmt.Sprintf("Итого: %f\n", totalSum))
	file.WriteString("-------------------------------------------\n")
	file.WriteString("======================================================\n")
	c.JSON(http.StatusOK, gin.H{"status": "ok", "data": req, "totalSum": totalSum})
}
