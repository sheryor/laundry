package main

import (
	"laundry/laundry_rest"
	"laundry/laundrymodels"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

var configs laundrymodels.Config

func main() {
	router := gin.Default()
	// v1 group will need from connecting to middleware to handle token jwt
	v1 := router.Group("v1").Use(cors.Default())
	{
		v1.GET("/ping", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"message": "pong"})
		})
		v1.GET("/allprices", laundry_rest.GetConfig)
		v1.POST("/checkout", laundry_rest.Checkout)
	}
	router.Run()
}
